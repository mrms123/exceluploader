package citiTimesheet.ExcelUploader.model;

import java.sql.Date;
import java.sql.Time;

public class Timesheet {
	private Date date_field;
	private String team_member;
	private String project;
	private String module;
	private String phase;
	private String activity;
	private String description;
	private String ticket_id;
	private Float total_hours;
	private Time time_spent;

	public Date getDate_field() {
		return date_field;
	}

	public void setDate_field(Date date_field) {
		this.date_field = date_field;
	}

	public String getTeam_member() {
		return team_member;
	}

	public void setTeam_member(String team_member) {
		this.team_member = team_member;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	public Float getTotal_hours() {
		return total_hours;
	}

	public void setTotal_hours(Float total_hours) {
		this.total_hours = total_hours;
	}

	public Time getTime_spent() {
		return time_spent;
	}

	public void setTime_spent(Time time_spent) {
		this.time_spent = time_spent;
	}

}
