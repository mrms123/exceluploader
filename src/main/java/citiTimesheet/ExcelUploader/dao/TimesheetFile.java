package citiTimesheet.ExcelUploader.dao;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;

public class TimesheetFile {
	private String filePath;
	private int rowOffset;
	
	public TimesheetFile() {	
	}
	
	public TimesheetFile(String filePath) { 
		this.filePath = filePath;
	}
	
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilePath() {
		return filePath;
	}
	
	public int getRowOffset() {
		return rowOffset;
	}

	public void setRowOffset(int rowOffset) {
		this.rowOffset = rowOffset;
	}

	public boolean isValidPath() {
		try {
			Paths.get(filePath);
			
		} catch (InvalidPathException ex) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}

	public boolean checkfileFormat() {

		String filename = new File(filePath).getName();
		String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
		String excel = "xlsx";
		if (!extension.equals(excel)) {
			return false;
		} else {
			return true;
		}

	}

	

	
}
