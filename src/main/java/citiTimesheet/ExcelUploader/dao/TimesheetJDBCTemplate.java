package citiTimesheet.ExcelUploader.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;

import citiTimesheet.ExcelUploader.dao.TimesheetDAO;
import citiTimesheet.ExcelUploader.model.Timesheet;

public class TimesheetJDBCTemplate implements TimesheetDAO {
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void readExcelFile(TimesheetFile bla) {
		try {
			// FileInputStream fileInput = new FileInputStream(new
			// File("C:/Users/priyanka.tuteja/Desktop/OpRisk_TimeSheet.xlsx"));

			FileInputStream fileInput = new FileInputStream(new File(bla.getFilePath()));
			XSSFWorkbook workBook = new XSSFWorkbook(fileInput);
			XSSFSheet sheett = workBook.getSheetAt(0);
			Iterator<Row> rowIterator = sheett.iterator();
			List<Timesheet> list = new ArrayList<Timesheet>();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				if (row.getRowNum() < bla.getRowOffset()) {
					continue;
				}

				Iterator<Cell> cellIterator = row.cellIterator();
				Timesheet sheet = new Timesheet();

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch (cell.getColumnIndex()) {
					case 0:
						sheet.setDate_field(new java.sql.Date(row.getCell(9).getDateCellValue().getTime()));
						break;
					case 1:
						sheet.setTeam_member(row.getCell(1).getStringCellValue());
						break;
					case 2:
						sheet.setProject(row.getCell(2).getStringCellValue());
						break;
					case 3:

						sheet.setModule(row.getCell(3).getStringCellValue());
						break;
					case 4:

						sheet.setPhase(row.getCell(4).getStringCellValue());
						break;
					case 5:
						sheet.setActivity(row.getCell(5).getStringCellValue());
						break;
					case 6:
						sheet.setDescription(row.getCell(6).getStringCellValue());
						break;
					case 7:
						sheet.setTicket_id(row.getCell(7).getStringCellValue());
						break;

					case 8:
						sheet.setTotal_hours((float) row.getCell(8).getNumericCellValue());
						break;
					case 9:
						sheet.setTime_spent(new java.sql.Time(row.getCell(9).getDateCellValue().getTime()));
						break;

					}

				}

				list.add(sheet);
				// save(employee);
			}
			saveExcelFile(list);
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public Boolean saveExcelFile(final List<Timesheet> sheet) {

		String query = "INSERT INTO citi_timesheet"
				+ "(date_field, team_member, project, module, phase, activity, description, ticket_id, total_hours, time_spent) VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?)";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		return jdbcTemplate.execute(query, new PreparedStatementCallback<Boolean>() {

			public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException {
				for (final Timesheet data : sheet) {
					ps.setDate(1, (Date) data.getDate_field());
					ps.setString(2, data.getTeam_member());
					ps.setString(3, data.getProject());
					ps.setString(4, data.getModule());
					ps.setString(5, data.getPhase());
					ps.setString(6, data.getActivity());
					ps.setString(7, data.getDescription());
					ps.setString(8, data.getTicket_id());
					ps.setFloat(9, data.getTotal_hours());
					ps.setTime(10, data.getTime_spent());

					ps.addBatch();

				}
				ps.executeBatch();
				return true;
			}
		});
	}

}
