package citiTimesheet.ExcelUploader.dao;
import java.util.List;
import citiTimesheet.ExcelUploader.model.Timesheet;

public interface TimesheetDAO {
		
		public void readExcelFile(TimesheetFile bla);
		public Boolean saveExcelFile(List<Timesheet> sheet);
		
}
