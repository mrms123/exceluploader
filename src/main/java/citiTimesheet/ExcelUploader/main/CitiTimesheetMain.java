package citiTimesheet.ExcelUploader.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*import acgPortal.saveExcelFile.ExcelUploader;*/
import citiTimesheet.ExcelUploader.dao.TimesheetDAO;
import citiTimesheet.ExcelUploader.dao.TimesheetJDBCTemplate;
import citiTimesheet.ExcelUploader.dao.TimesheetFile;

public class CitiTimesheetMain {
	public static void main(String[] args) {
		try {
			Scanner input = new Scanner(System.in);

			System.out.println("Enter the File Path : ");
			String filesPath = input.nextLine();

			System.out.println("Enter the number of rows you want to skip : ");
			Integer rowOffset = input.nextInt();
			
	//	TimesheetFile obj = new TimesheetFile(filesPath);
        //C:\Users\priyanka.tuteja\Desktop\OpRisk_TimeSheet.xlsx
		//	boolean isValid = obj.isValidPath();
			
			ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
			TimesheetFile obj1= (TimesheetFile) ctx.getBean("timesheetfile");
		//	obj1.TimesheetFile(filesPath);
			obj1.setFilePath(filesPath);
			boolean isValid = obj1.isValidPath();
			if (isValid) {
				boolean isValidFormat = obj1.checkfileFormat();
				if (isValidFormat) {
					obj1.setRowOffset(rowOffset);
					
					//ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
					TimesheetDAO timesheetDAO= (TimesheetDAO) ctx.getBean("timesheetJDBCTemplate");
				
					timesheetDAO.readExcelFile(obj1);
					
					ctx.close();
					System.out.println("DONE");
					
				} else {
					System.out.println(" Please upload .xlsx files only !");
				}
			} else {
				System.out.println("Invalid Path");
			}
			input.close();

		}/*catch (InvalidFormatException e) {
			System.err.println("ERROR : Please upload .xlsx files only !");
		}*/

		catch (InputMismatchException e) {
			System.err.println("ERROR : Invalid input !");
		}

	/*	catch (FileNotFoundException e) {
			System.err.println("ERROR : File not found !");
		}

		catch (IOException e) {

			System.err.println("ERROR : File Opening Failed!");

		}*/

		catch (NullPointerException e) {
			System.err.println("ERROR : A Null Pointer Exception was caught!");
		}

		catch (Exception e) {
			System.out.println(e);
		}

	}
		
}
		
		
		
		
		
	

